{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module HTMLSpec where

import Data.Bits ((.|.))
import Brick hiding (Result)
import Brick.Markup
import Data.Text.Markup
import qualified Data.Text as T
import Test.Hspec
import Text.Trifecta
import Text.Trifecta.Delta
import qualified Graphics.Vty as V
import Text.RawString.QQ

import Data.HTML

stickyCom = "The /g/ Wiki:<br><a href=\"http://wiki.installgentoo.com/\">http://wiki.installgentoo.com/</a><br><br>\r\n\r\n/g/is for the discussion of technology and related topics.<br>\r\n/g/ is<b><u>NOT</u></b> your personal tech support team or personal consumer review site.<br><br>\r\nFor tech support/issues with computers, use <a href=\"https://boards.4chan.org/wsr/\">/wsr/ - Worksafe Requests</a> or one of the following:<br>\r\n<a href=\"https://startpage.com/\">https://startpage.com/</a> or <a href=\"https://duckduckgo.com\">https://duckduckgo.com</a> (i.e., fucking google it)<br>\r\n<a href=\"https://stackexchange.com/\">https://stackexchange.com/</a><br>\r\n<a href=\"http://www.logicalincrements.com/\">http://www.logicalincrements.com/< a><br><br>\r\n\r\nYou can also search the catalog for a specific term by using:<br>\r\n<a href=\"https://boards.4chan.org/g/searchword\"><a href=\"https://boards.4chan.org/g/searchword\" target=\"_blank\">https://boards.4chan.org/g/searchwo<wbr>rd</a></a> or by clicking on [Search]<br><br> \r\n\r\nAlways check the catalog before creating a thread:<br><a href=\"https://boards.4chan.org/g/catalog\"><a href=\"/g/catalog\" class=\"quotelink\">&gt;&gt;&gt;/g/catalog</a></a><br><br>\r\n\r\nPlease check the rules before you post:<br><a href=\"https://www.4chan.org/rules\"></a><br>\r\n<i>Begging for cryptocurrency is against the rules!</i><br><br>\r\n\r\nTo use the Code tag, book-end your body of code with: [code] and [/code]"

csgComment = "In /csg/, we discuss the cheap shit you see on Gearbest, Taobao, AliExpress, Banggood, eBay and similar sites.<br><br><span class=\"quote\">&gt;IRC channel</span><br>https://qchat.rizon.net/?channels=/<wbr>csg/<br>#/csg/ on rizon<br><br><span class=\"quote\">&gt;Discord link</span><br>https://discord.gg/bUJ3VGM<br><br><span class=\"quote\">&gt;Chink Shit Randomiser</span><br>http://chinkshit.xyz/random/<br><br><span class=\"quote\">&gt;Chink Shit Wiki</span><br>https://wiki.installgentoo.com/inde<wbr>x.php/Chink_shit_general<br><br><span class=\"quote\">&gt;Chink Shit Infographic </span><br>http://chinkshit.xyz/infographic.pd<wbr>f<br><br><span class=\"quote\">&gt;News</span><br>• Lazy Anon makes the shittiest, most cancerous /csg/ thread ever <a href=\"/g/thread/66756358#p66756358\" class=\"quotelink\">&gt;&gt;66756358</a><br>• Lazy anon is universally hated <a href=\"/g/thread/66756358#p66756789\" class=\"quotelink\">&gt;&gt;66756789</a><br>• Goolag is trying jew tricks on our beloved chink phones <a href=\"/g/thread/66756358#p66756450\" class=\"quotelink\">&gt;&gt;66756450</a> <a href=\"/g/thread/66756358#p66756623\" class=\"quotelink\">&gt;&gt;66756623</a><br>• Anon orders a housefire battery <a href=\"/g/thread/66756358#p66759604\" class=\"quotelink\">&gt;&gt;66759604</a><br>• Anon prevents a chink battery initiated housefire <a href=\"/g/thread/66756358#p66762530\" class=\"quotelink\">&gt;&gt;66762530</a><br>• Chinky price gouging continues <a href=\"/g/thread/66756358#p66764072\" class=\"quotelink\">&gt;&gt;66764072</a><br>• Anodda bulging battery <a href=\"/g/thread/66756358#p66764590\" class=\"quotelink\">&gt;&gt;66764590</a><br>• Anon joins the chink phone club <a href=\"/g/thread/66756358#p66768757\" class=\"quotelink\">&gt;&gt;66768757</a><br>• Anons still curious about putin cards <a href=\"/g/thread/66756358#p66771534\" class=\"quotelink\">&gt;&gt;66771534</a> <a href=\"/g/thread/66756358#p66770061\" class=\"quotelink\">&gt;&gt;66770061</a><br>• Anon got a mixza dog sd card <a href=\"/g/thread/66756358#p66770637\" class=\"quotelink\">&gt;&gt;66770637</a> turns out to be slow as shit <a href=\"/g/thread/66756358#p66771154\" class=\"quotelink\">&gt;&gt;66771154</a><br>• uGreen QC3 car charger seems like a good buy <a href=\"/g/thread/66756358#p66765622\" class=\"quotelink\">&gt;&gt;66765622</a><br>• Nice list of five dorrah chink products to buy <a href=\"/g/thread/66756358#p66756676\" class=\"quotelink\">&gt;&gt;66756676</a><br><br><br><span class=\"quote\">&gt;Last thread</span><br><a href=\"/g/thread/66756358#p66756358\" class=\"quotelink\">&gt;&gt;66756358</a>"

boldAndUnderline = "<u>underline <b>bold and underlined!"

urlAnchor = "<a href=\"http://google.co.uk\">google</a>"
urlAnchor2 = "<a href=\"https://boards.4chan.org/g/catalog\">"

urlLink = "<a href=\"http://boring-website.com\">link</a>"

urlCrossLink = "<a href=\"/g/catalog\" class=\"quotelink\">/g/</a>"

parsedSticky :: Result [(T.Text, V.Attr)]
parsedSticky = parseString (markupParser V.defAttr) mempty stickyCom

spec :: Spec
spec = describe "Data.HTML" $ do

    it "test parseBold" $ do
        let Success a = parseString (parseBold V.defAttr) mempty "<b>"
        a `shouldBe` (V.defAttr `V.withStyle` V.bold)
        let Failure (ErrInfo _ a) =
                parseString (parseBold V.defAttr) mempty "<c>"
        a `shouldBe` [Columns 0 0]

    it "test parseTag" $ do
        let result =
                parseString (parseTag V.defAttr) mempty "<br>"
        case result of
            Success x -> x `shouldBe` Left "<br>"
            (Failure _) -> do
                print result
                expectationFailure "parsing failed"

    it "test parseOpenURL" $ do
        let r = parseString (parseOpenURL V.defAttr) mempty urlAnchor2
        print r

    it "build markup" $ do
        let b = markupParser V.defAttr
            Success nothing = parseString b mempty "abcdefg"
        nothing `shouldBe` [("abcdefg", V.defAttr)]

    it "build markup with bold" $ do
        let noStyle = V.defAttr {V.attrStyle = V.SetTo 0}
            b = markupParser noStyle
            Success markup = parseString b mempty
                "not bold <b>bold</b> not bold"
        markup `shouldBe`
            [ ("not bold ", noStyle)
            , ("bold", noStyle `V.withStyle` 0x20)
            , (" not bold", noStyle)]

    it "test bold and underline" $ do
        let noStyle = V.defAttr {V.attrStyle = V.SetTo 0}
            Success result =
                parseString (markupParser noStyle) mempty boldAndUnderline
        result `shouldBe`
            [ ("", noStyle)
            , ("underline ", noStyle `V.withStyle` V.underline)
            , ("bold and underlined!",
                noStyle `V.withStyle` (V.underline .|. V.bold))]

    it "test url parsing" $ do
        let r = parseString (markupParser V.defAttr) mempty urlLink
            p = createMarkup (T.pack urlLink)
        print (fmap markupToList p)
        case r of
          Success x -> print ""
          Failure x -> print x

    it "test url cross link parsing" $ do
        let r = parseString (markupParser V.defAttr) mempty urlCrossLink
        print r

    it "test parsing sticky comment" $ do
        let r = parseString (markupParser V.defAttr) mempty stickyCom
        case r of
            Success x -> print "Succeeded"
            Failure err -> do
                print err
                expectationFailure ""

    it "test csg comment" $ do
        let r = parseString (markupParser V.defAttr)
                    mempty (T.unpack $ htmlToText csgComment)
        case r of
            Success x -> do
                print "Succeeded"
                let Just mkup = createMarkup (htmlToText csgComment)
                mapM_ print $ markupToList mkup
            Failure err -> do
                print err
                expectationFailure ""

    it "test createMarkup" $ do
        let r = parseString (markupParser V.defAttr) mempty stickyCom
        -- print r
        case r of
            Success x -> do
                let m = markupToList <$> createMarkup (T.pack stickyCom)
                print m
            Failure x -> expectationFailure "Parsing failed"
