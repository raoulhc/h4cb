module WrapMarkup where

import Data.Char (isSpace)
import Control.Lens ((^.), (&), (.~), (%~), (^?!), _last)
import Control.Monad (forM)
import Brick
import Brick.Markup
import qualified Data.Text as T
import Data.Text.Markup
import Text.Wrap
import qualified Graphics.Vty as V

markupWrap :: (Eq a, GetAttr a) => Markup a -> Widget n
markupWrap = markupWrapWith defaultWrapSettings

-- | Returns widget with wrapped markup lines
markupWrapWith :: (Eq a, GetAttr a) => WrapSettings -> Markup a -> Widget n
markupWrapWith settings m = Widget Greedy Fixed $ do
    c <- getContext
    let markupLines = markupToList m
        stripEndSettings = settings { stripEndWS = False }
        wrappedLines = concatMap (wrapMarkup stripEndSettings (c^.availWidthL))
            markupLines
        mkLine pairs = do
            is <- forM pairs $ \(t, aSrc) -> do
                a <- getAttr aSrc
                return $ V.string a $ T.unpack t
            return $ V.horizCat is
    lineImgs <- mapM mkLine wrappedLines
    return $ emptyResult & imageL .~ V.vertCat lineImgs

-- | function that wraps markup to lines of set length
wrapMarkup :: WrapSettings -> Int -> [(T.Text, a)] -> [[(T.Text, a)]]
wrapMarkup settings lmax line = snd $ foldl (flip accF) (0, [[]]) line
  where
    -- accumulator with column number
    accF (itxt, a) (cl, states)
        | n + cl > lmax = case ntxt of
            [] -> (0, states)
            [x] -> (T.length x, states)
            (x:_) -> let rest = wrap lmax (T.dropWhile isSpace
                            . T.drop (T.length x) $ itxt)
                in (T.length $ ntxt ^?! _last, states
                & (_last %~ (++ [(x, a)])) -- append to previous line
                & (++ fmap (\t -> [(t, a)]) rest)) -- append following lines
        | otherwise = (n + cl, states & _last %~ (++ [(itxt, a)]))
      where
        n = T.length itxt
        ntxt = wrap (lmax - cl) itxt
        wrap = wrapTextToLines settings
