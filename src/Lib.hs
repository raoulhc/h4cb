{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Control.Monad.IO.Class (liftIO)
import Control.Monad (void)
import Brick
import Brick.Widgets.Center
import Brick.Widgets.Border
import Brick.Widgets.Border.Style
import Brick.Themes
import qualified Data.Text as T
import Data.Text.Markup
import qualified Data.Vector as Vc
import Data.List (intersperse)
import Brick.Widgets.List
import Data.Maybe (fromMaybe)
import qualified Graphics.Vty as V
import Text.Wrap

import WrapMarkup
import Data.HTML
import Data.Thread
import Fetch

data AppState =
    SEntry (List T.Text (T.Text, T.Text))
  | SCatalog (List T.Text OP)
  | SThread T.Text Int Thread
  deriving Show

defaultTheme :: Theme
defaultTheme = newTheme (V.white `on` V.black) []

selectedBorder :: [(AttrName, V.Attr)]
selectedBorder =
    [(borderAttr, fg V.brightMagenta)]

opWidget :: Bool -> OP -> Widget T.Text
opWidget b op@(OP _ (Comment _ _ _ icom)) = hCenter . hLimit 80
    . (if b
        then updateAttrMap (applyAttrMappings selectedBorder)
        . withBorderStyle unicodeBold
        else id)
    . border . withBorderStyle defaultBorderStyle
    . vLimit 10 . padBottom Max
    $ opBar op
    <=> hBorder
    <=> markupWrapWith defaultWrapSettings (fromMaybe (fromList []) x)
  where x = icom >>= createMarkup

catWidget :: List T.Text OP -> Widget T.Text
catWidget = renderList opWidget True

opBar :: OP -> Widget T.Text
opBar (OP (Head subj repl img) (Comment n t num _)) =
    maybe emptyWidget
        (\x -> (hCenter . txtWrap) (mconcat [" ", x, " "]) <=> hBorder)
        subj
    <=>
    (hCenter . vLimit 1 . hBox . intersperse vBorder)
    [ txt $ " " <> n <> " "
    , txt $ " " <> t <> " "
    , str $ " No." <> show num <> " "
    , str $ " replies: " <> show repl <> " "
    , str $ " images: " <> show img <> " " ]

comWidget :: Comment -> Widget T.Text
comWidget (Comment iname itime inumber icom) = border $
    (hCenter . vLimit 1 . hBox . intersperse vBorder $
        [ txt $ " " <> iname <> " "
        , txt $ " " <> itime <> " "
        , str $ " No." <> show inumber <> " " ])
    <=> hBorder
    <=> markupWrapWith defaultWrapSettings (fromMaybe (fromList []) x)
  where x = icom >>= createMarkup

threadWidget :: Thread -> Widget T.Text
threadWidget (Thread ihead icoms) = hCenter . hLimit 80
    $ border (opBar (OP ihead (head icoms)))
    <=> hBorder
    <=> (viewport "viewport" Vertical . cached "viewport")
    (vBox (comWidget <$> icoms))

boards :: List T.Text (T.Text, T.Text)
boards = list "Boards" (Vc.fromList
    [ ("g", "Technology")
    , ("mu", "Music")
    , ("sci", "Science & Math")
    , ("lit", "Literature")
    , ("co", "Comics & Cartoons")
    , ("tv", "Television & Films")
    , ("v", "Video Games")]) 1

boardWidget :: Bool -- ^ focus
            -> (T.Text, T.Text)
            -> Widget T.Text
boardWidget sel (t1, t2) = vLimit 3 . border
    $ txt (" /" <> t1 <> "/ ")
    <+> vBorder <+> (if sel then vBorder else emptyWidget)
    <+> (padRight Max . txt) (" " <> t2 <> " ")

entryWidget :: List T.Text (T.Text, T.Text) -> Widget T.Text
entryWidget l = vCenter . hCenter . hLimit 40 $ (vLimit 20
    . borderWithLabel (txt "Welcome to h4cb")
    . renderList boardWidget True $ l)
    <=> hCenter (txt "Pick a board")

appEvent :: AppState -> BrickEvent T.Text e -> EventM T.Text (Next AppState)
appEvent s (VtyEvent (V.EvKey V.KEsc []))        = halt s
appEvent s (VtyEvent (V.EvKey (V.KChar 'q') [])) = halt s
appEvent s@SEntry{} (VtyEvent key)               = entryEvent s key
appEvent s@SCatalog{} (VtyEvent key)             = catalogEvent s key
appEvent s@SThread{} (VtyEvent key)              = threadEvent s key
appEvent s _                                     = continue s

entryEvent :: AppState -> V.Event ->  EventM T.Text (Next AppState)
entryEvent (SEntry l) (V.EvKey V.KEnter _) =
        let Just x = listSelectedElement l
            b = fst . snd $ x in loadCatalog b
entryEvent (SEntry l) event = handleListEvent event l >>= (continue . SEntry)
entryEvent s _ = continue s

catalogEvent :: AppState -> V.Event -> EventM T.Text (Next AppState)
catalogEvent (SCatalog l) (V.EvKey V.KEnter _) =
        let Just x = listSelectedElement l
            threadNo = number . opcomment . snd $ x
            b = listName l in do
        Just thread <- liftIO $ fetchThread b threadNo
        continue $ SThread b threadNo thread
catalogEvent _ (V.EvKey (V.KChar 'b') _) = continue (SEntry boards)
catalogEvent (SCatalog l) event =
        handleListEvent event l >>= (continue . SCatalog)
catalogEvent s _ = continue s

threadEvent :: AppState -> V.Event -> EventM T.Text (Next AppState)
threadEvent s@(SThread b _ _) (V.EvKey k _)
  | k `elem` [V.KDown, V.KChar 'j'] = scroll 1
  | k `elem` [V.KUp, V.KChar 'k']   = scroll (-1)
  | k == V.KPageDown                = scroll 10
  | k == V.KPageUp                  = scroll (-10)
  | k == V.KChar 'b'                = loadCatalog b
  | otherwise                       = continue s
  where
      scroll x = vScrollBy vp x >> continue s
      vp = viewportScroll "viewport"
threadEvent s _ = continue s

loadCatalog :: T.Text -> EventM T.Text (Next AppState)
loadCatalog b = do
    Just ops <- liftIO $ fetchCatalog b
    continue $ SCatalog (list b (Vc.fromList ops) 2)

myApp :: App AppState () T.Text
myApp = App
    { appDraw         = myAppDraw
    , appStartEvent   = return
    , appHandleEvent  = appEvent
    , appAttrMap      = const $ themeToAttrMap defaultTheme
    , appChooseCursor = neverShowCursor }

myAppDraw :: AppState -> [Widget T.Text]
myAppDraw (SCatalog ops)       = [catWidget ops]
myAppDraw (SThread _ _ thread) = [threadWidget thread]
myAppDraw (SEntry l)           = [entryWidget l]

someFunc :: IO ()
someFunc = do
    let buildVty = do
            v <- V.mkVty =<< V.standardIOConfig
            V.setMode (V.outputIface v) V.Hyperlink True
            return v
    void $ customMain buildVty Nothing myApp (SEntry boards)
