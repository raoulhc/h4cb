{-# LANGUAGE OverloadedStrings #-}

module Fetch where

import Data.Text (Text(), unpack)
import Control.Lens ((^.))
import Data.Aeson
import Network.Wreq (get, responseBody)

import Data.Thread

fetchCatalog :: Text -> IO (Maybe [OP])
fetchCatalog board = do
    r <- get $ concat ["http://a.4cdn.org/", unpack board, "/catalog.json"]
    let mjsonCatalog = decode $ r ^. responseBody :: Maybe [Value]
        threads = mjsonCatalog >>= getAllOPs
    return threads

fetchThread :: Text -> Int -> IO (Maybe Thread)
fetchThread board no = do
    r <- get $ concat
        ["http://a.4cdn.org/", unpack board, "/thread/", show no, ".json"]
    return $ (decode $ r ^. responseBody :: Maybe Value) >>= getFullThread
