{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.HTML where

import Control.Applicative
import Data.Bits ((.&.), complement)
import Data.Text (Text)
import qualified Data.Text as T
import Brick.Markup
import Data.Text.Markup
import qualified Graphics.Vty as V
import Text.Trifecta
import Text.Parser.LookAhead

-- | Converts html tags and codes to text expressions, e.g.
-- htmlToText "&lt;&gt;" = "<>"
htmlToText :: Text -> Text
htmlToText = symbols . newLines

-- | helper function easily specify lists of text to replace with other strings
replaceList :: [(Text, Text)] -> (Text -> Text)
replaceList l = foldr1 (.) $ uncurry T.replace <$> l

-- | replace new line characters and word break opportunity tag
newLines :: Text -> Text
newLines = replaceList
    [ ("<br>", "\n")
    , ("<wbr>", "")
    , ("\n", "")
    , ("\r", "")]

-- | replace html codes with respective symbols
symbols :: Text -> Text
symbols = replaceList
    [ ("&gt;", ">")
    , ("&lt;", "<")
    , ("&#039;", "'")
    , ("&quot;", "\"")
    , ("&amp;", "&")]

-- | helper function to remove a style
removeStyle :: V.Attr -> V.Style -> V.Attr
removeStyle attr 0 = attr
removeStyle attr style =
    attr { V.attrStyle = V.SetTo $ V.styleMask attr .&. complement style}

-- TODO probably want to get it to output some sort of error if it doesn't work
-- later
-- | Create markup from raw text
createMarkup :: Text -> Maybe (Markup V.Attr)
createMarkup t =
    case parseString (markupParser V.defAttr) mempty (T.unpack t) of
        Success a -> Just (fromList a)
        Failure _ -> Nothing

-- | Recursive parser that returns text with attributes
markupParser :: V.Attr -> Parser [(Text, V.Attr)]
markupParser attr = do
    x <- many (notChar '<')
    y <- option [] $ do
        tag <- parseTag attr
        case tag of
            Right a -> markupParser a
            Left txt -> do
                y <- markupParser attr
                return $ (txt, attr):y
    return $ (T.pack x, attr):y

-- | Parses anchor either returning a new attribute, or the anchor in text
-- form.
parseTag :: V.Attr -> Parser (Either Text V.Attr)
parseTag attr = do
    try . lookAhead $ char '<'
    (Right <$> (parseStyles attr
        <|> parseOpenURL attr
        <|> parseCloseURL attr))
        <|> (Left <$> parseText)

-- | Combination of the style parsers
parseStyles :: V.Attr -> Parser V.Attr
parseStyles attr =
    parseBold attr
        <|> parseUnderline attr
        <|> parseItalic attr
        <|> parseQuote attr

parseBold :: V.Attr -> Parser V.Attr
parseBold attr =
        (string "<b>" >> return (attr `V.withStyle` V.bold))
    <|> (string "</b>" >> return ( attr `removeStyle` V.bold))

parseUnderline :: V.Attr -> Parser V.Attr
parseUnderline attr =
        (string "<u>" >> return (attr `V.withStyle` V.underline))
    <|> (string "</u>" >> return (attr `removeStyle` V.underline))

-- | No italic option in Brick/Vty so return the original attr
parseItalic :: V.Attr -> Parser V.Attr
parseItalic attr = (string "<i>" <|> string "</i>") >> return attr

-- | Set color to green for quotes
parseQuote :: V.Attr -> Parser V.Attr
parseQuote attr = open <|> close
   where
      open = do
          string "<span class=\"quote\">"
          return $ attr `V.withForeColor` V.green
      close = do
          string "</span>"
          return $ attr {V.attrForeColor = V.Default}

-- | fall back case for when we don't have an appropriate parser
parseText :: Parser Text
parseText = do
    char '<'
    txt <- many (notChar '>')
    char '>'
    return $ T.pack . mconcat $ ["<", txt, ">"]

-- | parse URL open tag, looks for quotelink and target options
parseOpenURL :: V.Attr -> Parser V.Attr
parseOpenURL attr = do
    try (char '<' >> string "a ")
    string "href=\""
    href <- many (notChar '\"')
    string "\""
    options <- (try (lookAhead (char '>')) >> return [])
        <|> (char ' ' >> (crosslink <|> target) `sepBy` char ' ')
    char '>'
    let link = if "quotelink" `elem` options
        then "http://boards.4chan.org" ++ href
        else href
    return $ attr
        { V.attrURL = V.SetTo $ T.pack link
        , V.attrForeColor = V.SetTo V.magenta}
  where crosslink = try (string "class=\"quotelink\"") >> return "quotelink"
        target = try (string "target=\"_blank\"") >> return T.empty

parseCloseURL :: V.Attr -> Parser V.Attr
parseCloseURL attr = do
    try $ string "</a>"
    return $ attr
        { V.attrURL = V.Default
        , V.attrForeColor = V.Default }
