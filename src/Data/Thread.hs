{-# LANGUAGE OverloadedStrings #-}

module Data.Thread where

import Data.Maybe
import Control.Lens ((^?), (^..))
import Data.Aeson (Value(String, Number))
import Data.Aeson.Lens (values, key)
import Data.Scientific (toBoundedInteger)
import qualified Data.Text as T

import Data.HTML

-- TODO Probably want the head to conist of a comment + additional info and
-- then wont have duplicate records
data Head = Head
    { subject :: Maybe T.Text
    , replies :: Int
    , images :: Int
    } deriving (Eq, Show)

data Comment = Comment
    { name :: T.Text
    , time :: T.Text
    , number :: Int
    , comment :: Maybe T.Text
    } deriving (Eq, Show)

data Thread = Thread Head [Comment]
    deriving (Eq, Show)

data OP = OP
    { ophead :: Head
    , opcomment :: Comment }
    deriving (Eq, Show)

-- json aux conversion functions
-- | Get Int out of json Value
getInt :: Value -> Maybe Int
getInt (Number n) = toBoundedInteger n
getInt _ = Nothing

-- | Get Text out of json Value
getText :: Value -> Maybe T.Text
getText (String s) = Just s
getText _ = Nothing

-- page functions

jsonToHead :: Value -> Maybe Head
jsonToHead xs = do
    let get f str = xs ^? key str >>= f
        geti = get getInt
        sub = get getText "sub"
    rep <- geti "replies"
    img <- geti "images"
    return $ Head sub rep img

jsonToCom :: Value -> Maybe Comment
jsonToCom xs = do
    let get str f = xs ^? key str >>= f
    n <- get "name" getText
    t <- get "now" getText
    no <- get "no" getInt
    let com = htmlToText <$> get "com" getText
    return $ Comment n t no com

-- TODO find more efficient lensy way of doing this
-- | Get OP out of a it's structure in the catalog
jsonToOP :: Value -> Maybe OP
jsonToOP xs = do
    headInfo <- jsonToHead xs
    opCom <- jsonToCom xs
    return $ OP headInfo opCom

-- | Get thread head structures from a page
getHeads :: Value -> Maybe [OP]
getHeads xs = fmap (mapMaybe jsonToOP)
    (^.. values) <$> (xs ^? key "threads")

-- | Get all threads from catalog pages
getAllOPs :: [Value] -> Maybe [OP]
getAllOPs = fmap concat . traverse getHeads

getFullThread :: Value -> Maybe Thread
getFullThread xs = do
    (o:cs) <- (^.. values) <$> (xs ^? key "posts")
    (OP h o') <- jsonToOP o
    cs' <- traverse jsonToCom cs
    return $ Thread h (o':cs')
